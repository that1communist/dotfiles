{ config, pkgs, ... }:
{
imports = [
	./amdgpu.nix
	./amdcpu.nix
	./invidious.nix
];
hardware.keyboard.zsa.enable = true;

services.kanata = {
	keyboards.ergodox = {
		devices = ["/dev/input/by-id/usb-ZSA_Technology_Labs_Ergodox_EZ_5VoRX_MPxar-event-kbd"];
		extraDefCfg = ''
		process-unmapped-keys false
		concurrent-tap-hold yes
		'';
		config = builtins.readFile (./. + "/ergodox.kbd") + builtins.readFile (./. + "/chords.kbd");
	};
};

environment.systemPackages = with pkgs; [
	nheko
	portal
	bsnes-hd
	cemu
	chromium
	citrix_workspace
	clonehero
	discord
	dolphin-emu
	dorion
	element-desktop
	gnome-boxes
	gpt4all
	helix
	kdePackages.neochat
	keymapp
	kickoff
	firefox
	floorp
	libva-utils
	lmstudio
	mgba
	r2modman
	ryujinx
	spotifywm
	stoken
	swayimg
	teams-for-linux
	telegram-desktop
	vdpauinfo
	ventoy
	wdisplays
	wineWow64Packages.full
#	cosmic-osd
#	cosmic-settings
#	cosmic-store
#	gpt4all
#	python313Full
#	python313Packages.pip
#	python313Packages.virtualenv
#	vesktop
];

nixpkgs.config.permittedInsecurePackages = [
	"olm-3.2.16"
	"fluffychat-linux-1.23.0"
];

systemd.user.timers = {
	"startwork-sat" = {
		wantedBy = [ "timers.target" ];
		timerConfig = {
			OnCalendar = "Sat *-*-* 12:51:00";
			Unit = "startwork.service";
		};
	};
	"startwork-sun" = {
		wantedBy = [ "timers.target" ];
		timerConfig = {
			OnCalendar = "Sun *-*-* 10:51:00";
			Unit = "startwork.service";
		};
	};
	"startwork-montues" = {
		wantedBy = [ "timers.target" ];
		timerConfig = {
			OnCalendar = "Mon,Tue *-*-* 14:51:00";
			Unit = "startwork.service";
		};
	};
	"approvetimesheet" = {
		wantedBy = [ "timers.target" ];
		timerConfig = {
			OnCalendar = "Sun *-*-* 23:59:50";
			Unit = "approvetimesheet.service";
		};
	};
	"killwork" = {
		wantedBy = [ "timers.target" ];
		timerConfig = {
			OnCalendar = "Sun,Mon,Tue *-*-* 00:05:00";
			Unit = "killwork.service";
		};
	};
	"killwork-tuesday" = {
		wantedBy = [ "timers.target" ];
		timerConfig = {
			OnCalendar = "Wed *-*-* 02:05:00";
			Unit = "killwork.service";
		};
	};
};
systemd.user.services = {
	"killwork" = {
		script = ''
			set -u ;
			${pkgs.hyprland}/bin/hyprctl dispatch exec 'pkill -15 -f "electron.*teams-for-linux"' &
			${pkgs.hyprland}/bin/hyprctl dispatch exec 'pkill -15 wfica' &
			${pkgs.hyprland}/bin/hyprctl dispatch closewindow class:lmirtechconsole\.exe
		'';
		serviceConfig = {
			Type = "oneshot";
		};
	};
	"startwork" = {
		script = ''
			set -u ;
			${pkgs.hyprland}/bin/hyprctl dispatch exec firefox &
			${pkgs.hyprland}/bin/hyprctl dispatch exec teams-for-linux &
			${pkgs.hyprland}/bin/hyprctl dispatch exec wine "~/.wine/drive_c/Program\ Files\ \(x86\)/LogMeIn\ Rescue\ Technician\ Console/LogMeInRescueTechnicianConsole_x64/LMIRTechConsole.exe" &
			${pkgs.floorp}/bin/floorp -private-window remote.imf.org
		'';
		serviceConfig = {
			Type = "oneshot";
		};
	};
	"approvetimesheet" = {
		script = ''
			set -u ;
			${pkgs.hyprland}/bin/hyprctl dispatch exec notify-send "Submit your timesheet"
		'';
		serviceConfig = {
			Type = "oneshot";
		};
	};

};
}
