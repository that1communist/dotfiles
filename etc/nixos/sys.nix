{ config, lib, pkgs, ... }:
{
imports = [
	./${lib.removeSuffix "\n" (builtins.readFile /etc/hostname)}.nix
];
networking = {
	networkmanager.enable = lib.mkDefault false;
	nameservers = [ "1.1.1.1" "9.9.9.9" ];
	wireless.iwd = {
		enable = true;
		settings = {
			IPv6 = {
				Enabled = true;
			};
			Settings = {
				AutoConnect = true;
			};
		};
	};
};
nixpkgs.overlays = [
  (self: super: {
    mpv = super.mpv.override {
      scripts = [ self.mpvScripts.autosub ];
    };
})
 (final: prev: {
    adi1090x-plymouth-themes = prev.adi1090x-plymouth-themes.overrideAttrs (previousAttrs: {
      installPhase = previousAttrs.installPhase + ''
        find $out/share/plymouth/themes/ -name \*.script -exec sed -i 's/Window.GetX()/Window.GetX(0)/g' {} \;
        find $out/share/plymouth/themes/ -name \*.script -exec sed -i 's/Window.GetY()/Window.GetY(0)/g' {} \;
      '';
    });
  })
#(final: prev: { openssl = final.rustls-libssl; })
];
programs = {
	bat.enable = true;
	dconf.enable = true;
	hyprland = {
		enable = true;
		withUWSM = true;
	};
	hyprlock.enable = true;
	obs-studio.enable = true;
	seahorse.enable = true;
	ydotool = {
		enable = true;
		group = "wheel";
	};
	firefox = {
		enable = true;
		package = pkgs.librewolf;
		languagePacks = ["en-US"];
	};
	thunderbird.enable = true;
	gamescope = {
		enable = true;
		args = [
			"--rt"
			"--expose-wayland"
		];
	};
	steam = {
		package = pkgs.steam.override {
			extraPkgs = p: [
			p.posy-cursors
			];
		};
		enable = true;
		dedicatedServer.openFirewall = true;
		localNetworkGameTransfers.openFirewall = true;
		remotePlay.openFirewall = true;
		protontricks.enable = true;
		extraCompatPackages = with pkgs; [
			proton-ge-bin
		];
	};
	zsh = {
  		enable = true;
  		autosuggestions.enable = true;
  		syntaxHighlighting.enable = true;
  		interactiveShellInit = "source ${pkgs.zsh-history-substring-search}/share/zsh-history-substring-search/zsh-history-substring-search.zsh";
	};
	neovim = {
  		enable = true;
		defaultEditor = true;
		vimAlias = true;
	};
};
users.defaultUserShell = pkgs.zsh;
hardware = {
	bluetooth = {
		enable = true;
		powerOnBoot = true;
	};
	steam-hardware.enable = true;
	uinput.enable = true;
};
environment.systemPackages = with pkgs; [
	alacritty
	bato
	blueberry
	brightnessctl
	copyq
	deluge-gtk
	doas-sudo-shim
	dua
	easyeffects
	eza
	ffmpeg
	ffmpegthumbnailer
	flat-remix-icon-theme
	galculator
	gammastep
	git
	gnome-power-manager
	gnome-software
	gsettings-desktop-schemas
	handlr-regex
	hunspell
	hunspellDicts.en_US
	hyprkeys
	hyprland-autoname-workspaces
	hyprland-qtutils
	hyprsunset
	iwgtk
	jaq
	kdePackages.ark
	kdePackages.ffmpegthumbs
	kdePackages.gwenview
	kdePackages.merkuro
	kdePackages.okular
	kdePackages.qt6ct
	kdePackages.qtstyleplugin-kvantum
	killall
	lapce
	libappindicator
	libnotify
	libsForQt5.qt5ct
	libsForQt5.qtstyleplugin-kvantum
	libsecret
	lxqt.pcmanfm-qt
	mako
	mpv
	nodejs-slim
	p7zip
	pavucontrol
	pinta
	posy-cursors
	pulseaudio
	qbittorrent
	qtalarm
	resources
	ripgrep
	sd
	slurp
	sound-theme-freedesktop
	speedread
	swayidle
	swww
	syncplay
	tesseract
	themechanger
	tofi
	unrar
	vopono
	waybar
	wayland-pipewire-idle-inhibit
	waypaper
	wayshot
	wired
	wl-clipboard-rs
	wl-mirror
	wl-restart
	wljoywake
	xclip
	xdg-user-dirs
	xdg-utils
	zim
];
fonts = {
	packages = with pkgs; [
		font-awesome
		nerd-fonts.noto
		noto-fonts
		noto-fonts-cjk-sans
		noto-fonts-cjk-serif
		noto-fonts-lgc-plus
		noto-fonts-monochrome-emoji
		icomoon-feather
	];
	fontDir.enable = true;
	fontconfig = {
		defaultFonts = {
			monospace = [ "Noto Sans Mono Medium" ];
			sansSerif = [ "Noto Sans Medium" ];
			serif = [ "Noto Serif Medium" ];
			emoji = [ "Noto Emoji" ];
		};
		useEmbeddedBitmaps = true;
		cache32Bit = true;
	};
};
security = {
	rtkit.enable = true;
	sudo.enable = lib.mkDefault false;
	pam.services = {
		hyprlock.text = "auth include login";
		greetd.enableGnomeKeyring = true;
	};
	polkit = {
		enable = true;
		extraConfig = ''
			polkit.addRule(function(action, subject) {
				if (
					subject.isInGroup("users")
					&& (
						action.id == "org.freedesktop.login1.reboot" ||
						action.id == "org.freedesktop.login1.reboot-multiple-sessions" ||
						action.id == "org.freedesktop.login1.power-off" ||
						action.id == "org.freedesktop.login1.power-off-multiple-sessions"
					)
				)
				{
					return polkit.Result.YES;
				}
			});
		'';
	};
	doas = {
		enable = true;
		extraRules = [{
			groups = [ "wheel" ];
			keepEnv = true;
			persist = true;
			noLog = true;
		}];
	};
};
xdg.icons.fallbackCursorThemes = [ "Posy_Cursor_Black" ];
xdg.portal = {
	enable = true;
	xdgOpenUsePortal = true;
	config = {
		common = {
			default = ["hyprland" "hyprland"];
			"org.freedesktop.impl.portal.Secret" = [ "gnome-keyring" ];
		};
	};
	extraPortals = [
		pkgs.xdg-desktop-portal-hyprland
		pkgs.xdg-desktop-portal-gtk
	];
};
systemd = {
	services.flatpak-repo = {
		wantedBy = [ "multi-user.target" ];
		path = [ pkgs.flatpak ];
		script = ''
			flatpak remote-add --user --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
		'';
	};
	user.services.polkit-gnome-authentication-agent-1 = {
		description = "polkit-gnome-authentication-agent-1";
		wantedBy = [ "graphical-session.target" ];
		wants = [ "graphical-session.target" ];
		after = [ "graphical-session.target" ];
		serviceConfig = {
			Type = "simple";
			ExecStart = "${pkgs.polkit_gnome}/libexec/polkit-gnome-authentication-agent-1";
			Restart = "on-failure";
			RestartSec = 1;
			TimeoutStopSec = 10;
		};
	};
};
services = {
	fstrim.enable = true;
	playerctld.enable = true;
	systembus-notify.enable = true;
	fwupd.enable = true;
	dbus.implementation = "broker";
	journald.storage = "none";
	kanata = {
		enable = true;
		keyboards.all = {
			extraDefCfg = ''
				process-unmapped-keys false
				concurrent-tap-hold yes
				linux-device-detect-mode keyboard-only
				linux-dev-names-exclude (
					"ZSA Technology Labs Ergodox EZ"
					"Paris Commune Keyboard"
					"Kathakali's S21+ Keyboard"
					"ydotoold virtual device"
				)
			'';
			config = builtins.readFile (./. + "/main.kbd") + builtins.readFile (./. + "/chords.kbd");
			port = 52612;
		};
	};
	gnome.gnome-keyring.enable = true;
	greetd = {
		enable = true;
		settings = rec {
			default_session = {
				command = "${pkgs.wl-restart}/bin/wl-restart -n 20 ${pkgs.hyprland}/bin/Hyprland > /dev/null";
				user = config.services.getty.autologinUser;
			};
		};
	};
	kmscon = {
		enable = true;
		hwRender = true;
		autologinUser = config.services.getty.autologinUser;
	};
	flatpak.enable = true;
	pipewire = {
		enable = true;
		alsa = {
			enable = true;
			support32Bit = true;
		};
		pulse.enable = true;
		jack.enable = true;
	};
	earlyoom = {
		enable = true;
		enableNotifications = true;
		freeMemThreshold = 2;
		reportInterval = 0;
		extraArgs = [
			"--avoid"
			"'(^|/)(pipewire|wireplumber|pipewire-pulse|firefox|librewolf|floorp|chromium|wfica|Xwayland|teams-for-linux|alacritty|nvim|gammastep|easyeffects|swayidle|hyprland-autoname-workspaces|wljoywake)$'"
			"--ignore"
			"'(^|/)(init|systemd|Hyprland|dbus-broker|dbus-broker-launch|earlyoom|mako|waybar)$'"
		];
	};
	gvfs = {
		enable = true;
		package = pkgs.gvfs;
	};
	power-profiles-daemon.enable = true;
	ananicy = {
		enable = true;
		package = pkgs.ananicy-cpp;
		rulesProvider = pkgs.ananicy-rules-cachyos;
	};
	udev.extraRules = ''
		SUBSYSTEM=="power_supply",ENV{POWER_SUPPLY_ONLINE}=="0",RUN+="${pkgs.power-profiles-daemon}/bin/powerprofilesctl set power-saver"
		SUBSYSTEM=="power_supply",ENV{POWER_SUPPLY_ONLINE}=="1",RUN+="${pkgs.power-profiles-daemon}/bin/powerprofilesctl set performance"
	'';
	logind.extraConfig = ''
		HandlePowerKey=ignore
		HandlePowerKeyLongPress=poweroff
	'';
};
qt = {
	enable = true;
	platformTheme = "qt5ct";
};
console = {
#	font = "ter-132n";
#	packages = [pkgs.terminus_font];
#	useXkbConfig = true;
	earlySetup = false;
};
boot = {
	plymouth = {
		enable = true;
		theme = "hexagon_red";
		themePackages = with pkgs; [
			(adi1090x-plymouth-themes.override {
			selected_themes = [ "hexagon_red" ];
			})
		];
	};
	consoleLogLevel = 0;
	initrd.verbose = false;
	kernelModules = [ "tcp_bbr" ];
	kernel.sysctl = {
		"fs.inotify.max_user_watches" = "100000";
		"kernel.printk" = "3 3 3 3";
		"kernel.sysrq" = 0;
		"net.core.default_qdisc" = "cake";
		"net.ipv4.conf.all.accept_source_route" = 0;
		"net.ipv4.conf.all.rp_filter" = 1;
		"net.ipv4.conf.all.send_redirects" = 0;
		"net.ipv4.conf.default.rp_filter" = 1;
		"net.ipv4.conf.default.send_redirects" = 0;
		"net.ipv4.icmp_ignore_bogus_error_responses" = 1;
		"net.ipv4.tcp_congestion_control" = "bbr";
		"net.ipv4.tcp_fastopen" = 3;
		"net.ipv4.tcp_mtu_probing" = 1;
		"net.ipv6.conf.all.accept_source_route" = 0;
		"vm.swappiness" = 10;
	};
	kernelParams = [
		"amd_pstate=active"
		"boot.shell_on_fail"
		"loglevel=3"
		"nmi_watchdog=0"
		"nowatchdog"
		"panic=5"
		"quiet"
		"rd.systemd.show_status=false"
		"rd.udev.log_level=3"
		"rd.udev.log_priority=3"
		"splash"
		"udev.log_priority=3"
	];
	kernelPackages = pkgs.linuxPackages_zen;
	loader = {
		timeout = 1;
		systemd-boot.consoleMode = "max";
	};
};
environment.variables.XDG_RUNTIME_DIR = "/run/user/$UID"; # set the runtime directory
environment.sessionVariables = {
	NIXOS_OZONE_WL = "1";
	MOZ_ENABLE_WAYLAND = "1";
	HOST = config.networking.hostName;
	PROTON_NO_WM_DECORATION = "1";
	WINE_NO_WM_DECORATION = "1";
};
fileSystems."/" = {
	options = [
		"noatime"
		"nodiratime"
	];
};
}
