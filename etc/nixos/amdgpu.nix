{ config, pkgs, lib, ... }:
{
services.xserver.videoDrivers = lib.mkDefault [ "modesetting" ];
hardware = {
	amdgpu.initrd.enable = lib.mkDefault true;
	graphics = {
		enable = lib.mkDefault true;
		enable32Bit = lib.mkDefault true;
	};
};
environment.systemPackages = with pkgs; [ lact ];
systemd = {
	packages = with pkgs; [ lact ];
	services.lactd.wantedBy = ["multi-user.target"];
};
}

